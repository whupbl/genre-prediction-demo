FROM python:3.7
WORKDIR /code
ENV FLASK_APP=app.py
ENV FLASK_RUN_HOST=0.0.0.0

COPY requirements.txt requirements.txt
RUN pip install -r requirements.txt

EXPOSE 5000
COPY vectorizer.pickle vectorizer.pickle
COPY model.pickle model.pickle
COPY templates templates
COPY app.py app.py
CMD ["flask", "run"]