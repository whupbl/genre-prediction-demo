"""Training and dumping models for future use"""

import pandas as pd
import ast
import re
from sklearn import linear_model
from sklearn.feature_extraction.text import CountVectorizer, TfidfVectorizer
from sklearn.preprocessing import MultiLabelBinarizer
from sklearn.multiclass import OneVsRestClassifier
from sklearn.multioutput import MultiOutputClassifier
from sklearn.ensemble import RandomForestClassifier, GradientBoostingClassifier
from sklearn.metrics import make_scorer, f1_score
from sklearn.model_selection import cross_val_score, train_test_split
from nltk.stem import PorterStemmer
from nltk.corpus import stopwords
from nltk.stem.snowball import SnowballStemmer
from nltk.downloader import download
import pickle


# Swapping contracted versions for regular ones
def decontract(sentence):
    sentence = re.sub(r"n\'t", " not", sentence)
    sentence = re.sub(r"\'re", " are", sentence)
    sentence = re.sub(r"\'s", " is", sentence)
    sentence = re.sub(r"\'d", " would", sentence)
    sentence = re.sub(r"\'ll", " will", sentence)
    sentence = re.sub(r"\'t", " not", sentence)
    sentence = re.sub(r"\'ve", " have", sentence)
    sentence = re.sub(r"\'m", " am", sentence)
    return sentence


# Removing punctuation marks
def removePunctuation(sentence):
    sentence = re.sub(r'[?|!|\'|"|#]', r'', sentence)
    sentence = re.sub(r'[.|,|)|(|\|/]', r' ', sentence)
    sentence = sentence.strip()
    sentence = sentence.replace("\n", " ")
    return sentence


# Removing numbers
def removeNumber(sentence):
    alpha_sent = ""
    for word in sentence.split():
        alpha_word = re.sub('[^a-z A-Z]+', '', word)
        alpha_sent += alpha_word
        alpha_sent += " "
    alpha_sent = alpha_sent.strip()
    return alpha_sent


# Removing stopwords
def removeStopWords(sentence):
    return ' '.join(x for x in sentence.split() if x not in stop)


# Applying stemming to sentences
def stemming(sentence):
    stemmer = SnowballStemmer("english")
    stemmedSentence = ""
    for word in sentence.split():
        stem = stemmer.stem(word)
        stemmedSentence += stem
        stemmedSentence += " "
    stemmedSentence = stemmedSentence.strip()
    return stemmedSentence


genres = ['history',
          'crime',
          'action',
          'drama',
          'thriller',
          'sci-fi',
          'musical',
          'family',
          'comedy',
          'sport',
          'war',
          'biography',
          'horror',
          'adventure',
          'mystery',
          'music',
          'western',
          'fantasy',
          'animation',
          'romance']

download('stopwords')
stop = set(stopwords.words('english'))
stop.add('br')


# Loading train data and applying all pre-processing functions to sentences
train_data = pd.read_csv('train.csv')

train_data.dialogue = train_data.dialogue.str.lower()

new_dialogue = []
for sen in train_data.dialogue:
    sen = decontract(sen)
    sen = removePunctuation(sen)
    sen = removeNumber(sen)
    sen = removeStopWords(sen)
    sen = stemming(sen)
    new_dialogue.append(sen)
train_data.dialogue = new_dialogue

print(train_data.head().dialogue)

train_data.genres = train_data.genres.apply(ast.literal_eval)


# Transforming targets with binarizer
mlb = MultiLabelBinarizer(classes=genres)
target = mlb.fit_transform(train_data.genres)


# Vectorizing processed sentences with TF-IDF
vectorizer = TfidfVectorizer(max_df=20000)
vectorizer_train = vectorizer.fit_transform(train_data.dialogue)

print(vectorizer_train.shape[1])


# Training one-vs-rest classifier based on Logistic Regression
clf = linear_model.LogisticRegression(max_iter=1000)
model = OneVsRestClassifier(clf)
model.fit(vectorizer_train, target)


# Dumping trained data (including vectorizer)
pickle.dump(vectorizer, open('vectorizer.pickle', 'wb'))
pickle.dump(model, open('model.pickle', 'wb'))
