# Genre Prediction Demo

This repository is for a demo project, that predicts movie genre based on a dialogue.

I'd like to thank:

- [Python](https://www.python.org/) (obviously)
- [scikit-learn](https://scikit-learn.org/) for training the model and making predictions
- [Flask](https://palletsprojects.com/p/flask/) for all the webing and paging stuff
- and of course my best guy [Docker](https://www.docker.com/) for beautiful containers
