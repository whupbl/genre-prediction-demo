"""Main app: gets the data and makes a prediction"""

import re
from flask import Flask, render_template, flash, request
import pickle
from nltk.corpus import stopwords
from nltk.stem.snowball import SnowballStemmer
from nltk.downloader import download


# Swapping contracted versions for regular ones
def decontract(sentence):
    sentence = re.sub(r"n\'t", " not", sentence)
    sentence = re.sub(r"\'re", " are", sentence)
    sentence = re.sub(r"\'s", " is", sentence)
    sentence = re.sub(r"\'d", " would", sentence)
    sentence = re.sub(r"\'ll", " will", sentence)
    sentence = re.sub(r"\'t", " not", sentence)
    sentence = re.sub(r"\'ve", " have", sentence)
    sentence = re.sub(r"\'m", " am", sentence)
    return sentence


# Removing punctuation marks
def removePunctuation(sentence):
    sentence = re.sub(r'[?|!|\'|"|#]', r'', sentence)
    sentence = re.sub(r'[.|,|)|(|\|/]', r' ', sentence)
    sentence = sentence.strip()
    sentence = sentence.replace("\n", " ")
    return sentence


# Removing numbers
def removeNumber(sentence):
    alpha_sent = ""
    for word in sentence.split():
        alpha_word = re.sub('[^a-z A-Z]+', '', word)
        alpha_sent += alpha_word
        alpha_sent += " "
    alpha_sent = alpha_sent.strip()
    return alpha_sent


# Removing stopwords
def removeStopWords(sentence):
    return ' '.join(x for x in sentence.split() if x not in stop)


# Applying stemming to sentences
def stemming(sentence):
    stemmer = SnowballStemmer("english")
    stemmedSentence = ""
    for word in sentence.split():
        stem = stemmer.stem(word)
        stemmedSentence += stem
        stemmedSentence += " "
    stemmedSentence = stemmedSentence.strip()
    return stemmedSentence


genres = ['history',
          'crime',
          'action',
          'drama',
          'thriller',
          'sci-fi',
          'musical',
          'family',
          'comedy',
          'sport',
          'war',
          'biography',
          'horror',
          'adventure',
          'mystery',
          'music',
          'western',
          'fantasy',
          'animation',
          'romance']

download('stopwords')
stop = set(stopwords.words('english'))
stop.add('br')


# Loading Flask app and models
app = Flask(__name__)
vectorizer = pickle.load(open('vectorizer.pickle', 'rb'))
model = pickle.load(open('model.pickle', 'rb'))


@app.route('/', methods=['GET', 'POST'])
def prediction_form():
    if request.method == 'POST':
        # GET input text and transform it
        text = request.form['text']
        sen = decontract(text)
        sen = removePunctuation(sen)
        sen = removeNumber(sen)
        sen = removeStopWords(sen)
        sen = stemming(sen)

        vectorized_data = vectorizer.transform([sen])

        # Get prediction
        prediction = model.predict(vectorized_data)[0]

        # Get classes names and generate response
        classes = [genres[i] for i, v in enumerate(prediction) if v == 1]
        if len(classes) == 0:
            prediction_txt = "I honestly don't know. :("
        elif len(classes) == 1:
            prediction_txt = f"I think this dialogue is from some {classes[0]} movie."
        else:
            prediction_txt = f"I think this dialogue is from some {'-'.join(classes)} movie."

        return render_template('prediction_page.html', response_text=prediction_txt, request_text=text)

    # If GET return template page
    else:
        return render_template('prediction_page.html')
